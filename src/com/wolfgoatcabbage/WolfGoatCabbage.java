package com.wolfgoatcabbage;

public class WolfGoatCabbage {

    public static void letsDrawSolution(String[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
    }

    public static void main(String[] args) {
        System.out.println("STEP #1");
        System.out.println("Man takes goat to the right coast.");
        letsDrawSolution(GOAT);
        letsDrawSolution(BOATTOTHERIGHT);
        System.out.println();
        System.out.println("Wolf and cabbage stay on the left coast.");
        letsDrawSolution(WOLF);
        letsDrawSolution(CABBAGE);
        System.out.println();
        System.out.println();

        System.out.println("STEP #2");
        System.out.println("Man leaves goat on the right coast and returns to the left coast. He takes cabbage and directs the boat toward the right coast");
        letsDrawSolution(CABBAGE);
        letsDrawSolution(BOATTOTHERIGHT);
        System.out.println("In this moment goat is crying on the right coast and missing cabbage, wolf on the left coast is missing goat.");
        letsDrawSolution(GOAT);
        letsDrawSolution(WOLF);
        System.out.println();
        System.out.println();

        System.out.println("STEP #3");
        System.out.println("Man leaves cabbage on the right coast and takes goat to the left coast.");
        letsDrawSolution(CABBAGE);
        System.out.println("In this moment goat enjoys traveling to the left coast, wolf on the left coast still missing goat.");
        letsDrawSolution(GOAT);
        letsDrawSolution(BOATTOTHELEFT);
        letsDrawSolution(WOLF);
        System.out.println();
        System.out.println();

        System.out.println("STEP #4");
        System.out.println("Man takes wolf and leaves goat.");
        letsDrawSolution(WOLF);
        letsDrawSolution(BOATTOTHERIGHT);
        System.out.println("Goat stays on the left coast alone.");
        letsDrawSolution(GOAT);
        System.out.println("Cabbage wait on the right coast.");
        letsDrawSolution(CABBAGE);
        System.out.println();
        System.out.println();

        System.out.println("STEP #5");
        System.out.println("Man returns and takes goat to the right coast.");
        letsDrawSolution(GOAT);
        letsDrawSolution(BOATTOTHERIGHT);
        System.out.println();
        System.out.println();
        System.out.println("After half a day man moved them across the river.");
        letsDrawSolution(GOAT);
        letsDrawSolution(CABBAGE);
        letsDrawSolution(WOLF);
        System.out.println();
        System.out.println("THE END!");
    }

    public static final String[] WOLF = {
            "         |\\_/|         ",
            "        / o o\\         ",
            "        /(   )\\        ",
            "        / \\#/ \\       ",
            "        |     |         ",
            "        | | | |         ",
            "      (~\\ | | /~)      ",
            "       \\_|| ||_/       ",
            "       //_| |_\\\\      ",
    };

    public static final String[] CABBAGE = {
            "        .-~~~~-.        ",
            "       /  ( ( ' \\      ",
            "      | ( )   )  |      ",
            "      \\ ) ' }  / /     ",
            "      (` \\ , /  ~)     ",
            "       `-.`\\/_.-'      ",
            "          `\"\"         ",
    };

    public static final String[] GOAT = {
            "            ,,~~--___---,          ",
            "           /            .~,        ",
            "     /  _,~             )          ",
            "    (_-(~)   ~, ),,,(  /'          ",
            "     Z6  .~`' ||     \\ |          ",
            "     /_,/     ||      ||           ",
            "              W`      W`           ",
    };

    public static final String[] BOATTOTHERIGHT = {
            "    ___________________________________________    ",
            "   |........................................../    ",
            "   |........................................./     ",
            "    \\......................................./     ",
            "   _,~')_,~')_,~')_,~')_,~')_,~')_,~')_,~')/,~')_  ",
    };

    public static final String[] BOATTOTHELEFT = {
            "    ___________________________________________    ",
            "   \\..........................................|    ",
            "    \\.........................................|     ",
            "     \\......................................./     ",
            "   _,~')_,~')_,~')_,~')_,~')_,~')_,~')_,~')/,~')_  ",
    };
}

